using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Tag serialization of class variables
//#NOTE: Research keyword in system -JW
[System.Serializable]
//Container for JSON input top level
//Expected ColorList first entry
public class RootObject
{
    
    public List<ColorObject> colorList;

}

[System.Serializable]
//Container for JSON String entries 2nd level
public class ColorObject
{
    //Color Name
    public string name;
    //Color rgb data & defining slot
    //expected four rows
    public float[] color;

}