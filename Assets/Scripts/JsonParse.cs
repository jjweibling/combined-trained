using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using LINQ;
//using Newtonsoft.Json;


public class JsonParse : MonoBehaviour
{

    //Define input text file. Presume Unity connection
    public TextAsset jsonInput;

    void Start()
    {
        ///Test Reception of JSON Root Object
        //Debug.Log(jsonColor(jsonInput.text));
        //^Redundant

        //Retrieve root object
        RootObject tmpObject = jsonColor(jsonInput.text);

        //Index and print to console all row data
        for (int i = 0; i < tmpObject.colorList.Count; i++)
        {
            Debug.Log($"{tmpObject.colorList[i].name}:{tmpObject.colorList[i].color[0]}:{tmpObject.colorList[i].color[1]}:{tmpObject.colorList[i].color[2]}:{tmpObject.colorList[i].color[3]}");

/*
 *          print every data point for each row
            for (int j = 0; j < tmpObject.colorList[i].color.Length; j++)
            {
                Debug.Log(tmpObject.colorList[i].color[j]);
            }
 */           

        }
    }

    //Parses JSON string from unity tools
    public RootObject jsonColor(string jsonString)
    {
        return JsonUtility.FromJson<RootObject>(jsonString);
    }


}
